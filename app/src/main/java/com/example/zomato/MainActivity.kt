package com.example.zomato;

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.zomato.Utils.Utility


class MainActivity : AppCompatActivity(), AdapterInterface {
    override fun itemClicked(position: Int) {
        val intent = Intent(this, RestaurantDetail::class.java)
        intent.putExtra(Utility.PARAMETER1, position)
        startActivity(intent)
    }

    val LOCATION_PERMISSION_CODE = 101

    private lateinit var wordViewModel: RestaurantViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = RestaurantListAdapter(this, this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?

        wordViewModel = ViewModelProvider(this).get(RestaurantViewModel::class.java)
        if (checkPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            wordViewModel.fetchRestaurants()
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_CODE
            )
        }

        wordViewModel.allRestaurant.observe(this, Observer { words ->
            // Update the cached copy of the words in the adapter.
            words?.let { adapter.setWords(it) }
        })
    }

    fun checkPermission(permission: String): Boolean {
        if (ContextCompat.checkSelfPermission(this, permission)
            == PackageManager.PERMISSION_GRANTED
        )
            return true
        else
            return false
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_PERMISSION_CODE) {
            if (grantResults.size > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                wordViewModel.fetchRestaurants()
                Toast.makeText(
                    this,
                    "Location Permission Granted",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}
