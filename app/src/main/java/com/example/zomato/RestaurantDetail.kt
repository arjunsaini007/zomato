package com.example.zomato;

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.zomato.Utils.Utility.Companion.PARAMETER1
import kotlinx.android.synthetic.main.rest_detail.*

class RestaurantDetail : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.rest_detail)
        var introViewPagerAdapter = IntroViewPagerAdapter(supportFragmentManager)
        viewPager.adapter = introViewPagerAdapter
    }
}
