package com.example.zomato

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class IntroViewPagerAdapter(supportFragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(supportFragmentManager) {
    override fun getItem(position: Int): Fragment {

        return newInstance("s")
    }

    override fun getCount(): Int {
        return 4
    }

    companion object {
        fun newInstance(title: String): Fragment {
            val fragment = IntroFragment()
            val args = Bundle()
            args.putString("title", title)
            fragment.arguments = args
            return fragment
        }
    }
}