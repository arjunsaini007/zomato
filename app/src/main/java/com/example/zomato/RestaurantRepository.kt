package com.example.zomato;
import androidx.lifecycle.LiveData

/**
 * Abstracted Repository as promoted by the Architecture Guide.
 * https://developer.android.com/topic/libraries/architecture/guide.html
 */
class RestaurantRepository(private val wordDao: RestaurantDao) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allWords: LiveData<List<Restaurant>> = wordDao.getAlphabetizedWords()

    suspend fun insert(restaurant: Restaurant) {
        wordDao.insert(restaurant)
    }
}
