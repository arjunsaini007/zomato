package com.example.zomato.network

import com.example.zomato.model.Example
import com.google.gson.JsonObject
import io.reactivex.Observable
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.http.Headers


interface WikiApiService {

    companion object {

        val BASE_URL="https://developers.zomato.com/api/v2.1/"

        fun create(): WikiApiService {

            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BASIC
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(logging)

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                    GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .client(httpClient.build())
                .build()

            return retrofit.create(WikiApiService::class.java)
        }
    }

    @Headers("user-key: f9971c85d05d363e8c5b97f63fb517f8")
    @GET("geocode")
    fun restaurants(@Query("lat") action: String,
                      @Query("lon") format: String):
            Observable<Example>

}