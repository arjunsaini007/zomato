package com.example.zomato

import android.app.Application
import android.location.Location
import android.util.Log
import androidx.annotation.NonNull
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.zomato.model.NearbyRestaurant
import com.example.zomato.network.WikiApiService
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch


class RestaurantViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: RestaurantRepository
    val allRestaurant: MutableLiveData<List<NearbyRestaurant>> = MutableLiveData()
    private lateinit var disposable: Disposable
    val mFusedLocationClient = LocationServices.getFusedLocationProviderClient(application)
    val wikiApiServe by lazy {
        WikiApiService.create()
    }

    init {
        val wordsDao = RestaurantRoomDatabase.getDatabase(application, viewModelScope).wordDao()
        repository = RestaurantRepository(wordsDao)
        val mLocationRequest = LocationRequest()
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
    }


    fun fetchRestaurants() {
        disposable = wikiApiServe?.restaurants("28.459497", "77.026634")
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())?.subscribe(
                { result ->
                    Log.e("result is ", "result " + result)
                    allRestaurant.value = result.nearbyRestaurants
                },
                { error -> Log.e("result is errror", "result error" + error) }
            )!!
    }

    fun insert(restaurant: Restaurant) = viewModelScope.launch {
        repository.insert(restaurant)
    }


    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }

    private fun getLastLocation() {
        mFusedLocationClient.lastLocation.addOnCompleteListener(
            OnCompleteListener<Location>() {
                @Override
                fun onComplete(@NonNull task: Task<Location>) {
                    val location = task.result
                    if (location != null){
                       Log.e("lat lon","lat lon is "+location)
                    }
                }
            }
        )
    }


}
