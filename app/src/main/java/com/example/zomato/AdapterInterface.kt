package com.example.zomato

interface AdapterInterface {
    fun itemClicked(position: Int)
}