package com.example.zomato

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.zomato.model.NearbyRestaurant
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recyclerview_item.view.*


class RestaurantListAdapter internal constructor(
    context: Context, adapterInterface: AdapterInterface
) : RecyclerView.Adapter<RestaurantListAdapter.WordViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private val adapterInterface: AdapterInterface = adapterInterface
    private var restaurantList = emptyList<NearbyRestaurant>() // Cached copy of restaurantList

    inner class WordViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val wordItemView: TextView = itemView.findViewById(R.id.textView)
        val restImage: ImageView = itemView.findViewById(R.id.iv)
        val ratingBar: RatingBar = itemView.findViewById(R.id.ratingBar)
        val rlMain: RelativeLayout = itemView.findViewById(R.id.rlMain)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return WordViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        val current = restaurantList.get(position)
        holder.wordItemView.text = current.restaurant.name
        Picasso.get().load(current.restaurant.featuredImage).into(holder.restImage)
        holder.ratingBar.rating = current.restaurant.userRating.aggregateRating.toFloat()
        holder.rlMain.setOnClickListener({
            adapterInterface.itemClicked(position)
        })
    }

    internal fun setWords(restaurants: List<NearbyRestaurant>) {
        this.restaurantList = restaurants
        notifyDataSetChanged()
    }

    override fun getItemCount() = restaurantList.size
}


